//
//  Constants.swift
//  Contact List
//
//  Created by Akshay Naithani on 11/10/19.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

let emptyImageName:String = "empty-profile-image"
let numberNotPresent:String = "Number not present"
let anonymousName:String = "Anonymous"

//MARK: Contact Table Constants
let contactCellId:String = "ContactTableViewCell"
let contactRowHeight:CGFloat = 100
//MARK: Color
let enableBtnColor:UIColor = UIColor.blue
let disableBtnColor:UIColor = UIColor.gray
