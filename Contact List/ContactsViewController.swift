//
//  ViewController.swift
//  Contact List
//
//  Created by Akshay on 10/10/19.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit
import ContactsUI
import Photos

struct Contacts{
    var name:String
    var contactNo:String
    var contactImage:UIImage
}

class ContactsViewController: UIViewController {

    @IBOutlet weak var contactListTableView: UITableView!
    @IBOutlet weak var previousDataLoadBtn: UIButton!
    @IBOutlet weak var nextDataLoadBtn: UIButton!
    
    private var contactsArray = [Contacts]()
    private var contactIndexArray = [Int:[Contacts]]()
    private var isPermissionEnabled = false
    private var lastContentOffset: CGFloat = 0
    private var presentIndex:Int = 0
    private var vSpinner: UIView? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.checkRequestAccess()
        
    }
    private func checkRequestAccess() {
        CNContactStore().requestAccess(for: .contacts) { (access, error) in
            self.isPermissionEnabled = access
            DispatchQueue.main.async {
                self.setTableItems()
            }
        }
    }
    
    @IBAction func loadData(_ sender: UIButton) {
        var shouldAnimate = false
        if sender.tag == 0 {
            if presentIndex > 0 && presentIndex < contactIndexArray.count{
                presentIndex = presentIndex - 1
                shouldAnimate = true
            }
        }else if sender.tag == 1 {
            if presentIndex >= 0 && presentIndex < contactIndexArray.count - 1{
                presentIndex = presentIndex + 1
                shouldAnimate = true
            }
        }
        if shouldAnimate {
            addRamdomDelayAndTableReload()
        }
    }
    
    private func addRamdomDelayAndTableReload() {
        let time:Int = Int(arc4random_uniform(3))
        self.showSpinner(onView: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(time)) {
            self.removeSpinner()
            self.contactListTableView.reloadData()
        }
    }
}
//MARK: Spinner Methods
extension ContactsViewController {
    //Making sections for adding maximun 10 items
    private func setTableItems() {
        if isPermissionEnabled == false {
            return
        }
        var section = -1
        for (i,contact) in fetchContacts.enumerated() {
            var name = contact.givenName
            if name == "" {
                name = anonymousName
            }
            name = "Name:\(name)"
            var contactNo: String
            if let phoneNo = contact.phoneNumbers.first{
                contactNo = phoneNo.value.stringValue
            }else{
                contactNo = numberNotPresent
            }
            contactNo = "Phone no:\(contactNo)"
            let contactImage: UIImage
            if let thumbnailImageData = contact.thumbnailImageData {
                contactImage = UIImage(data: thumbnailImageData)!
            }else{
                contactImage = UIImage(named: emptyImageName)!
            }
            let contactItem = Contacts(name: name, contactNo: contactNo, contactImage: contactImage)
            if i%10 == 0 {
                section = section + 1
                contactIndexArray[section] = [contactItem]
            }else{
                contactIndexArray[section]?.append(contactItem)
            }
        }
        addRamdomDelayAndTableReload()
    }
    
    private var fetchContacts: [CNContact] {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactThumbnailImageDataKey] as [Any]
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        var results: [CNContact] = []
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        return results
    }
}
//MARK: Spinner Methods
extension ContactsViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }

    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }

}
//MARK: TableViewDelegates and TableViewDataSource Methods
extension ContactsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if contactIndexArray[presentIndex] == nil{
            previousDataLoadBtn.enable(status: false)
            nextDataLoadBtn.enable(status: false)
            return 0
        }
        guard let contactSection = contactIndexArray[presentIndex] else {
            fatalError("Section out of bound")
        }
        previousDataLoadBtn.enable(status: presentIndex != 0)
        nextDataLoadBtn.enable(status: presentIndex != contactIndexArray.count - 1)
        return contactSection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: contactCellId, for: indexPath) as? ContactTableViewCell else {
            fatalError("Unable to load cell")
        }
        let contactInfo = contactIndexArray[presentIndex]![indexPath.row]
        cell.name.text = contactInfo.name
        cell.contactImage.contentMode = .scaleAspectFit
        cell.contactImage.image = contactInfo.contactImage
        cell.contactNo.text = "\(String(describing: contactInfo.contactNo))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return contactRowHeight
    }
    
}

