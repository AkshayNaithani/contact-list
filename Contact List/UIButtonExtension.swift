//
//  UIButtonExtension.swift
//  Contact List
//
//  Created by Akshay Naithani on 11/10/19.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

extension UIButton {
    func enable(status: Bool) {
        self.backgroundColor = status ? enableBtnColor : disableBtnColor
        self.isEnabled = status
    }
}
extension UIViewController {
}
